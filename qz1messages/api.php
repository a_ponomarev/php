<?php

  session_start();

  if (!isset($_SESSION['user'])) {
      $_SESSION['user'] = array();
  }

  require_once 'vendor/autoload.php';

  use Monolog\Logger;
  use Monolog\Handler\StreamHandler;

// create a log channel
  $log = new Logger('main');
  $log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
  $log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

  DB::$user = 'qz1messages';
  DB::$password = 'X6HeATo7EXUeJ4Qy';
  DB::$dbName = 'qz1messages';
  DB::$port = 3333;
  DB::$encoding = 'utf8';
  DB::$error_handler = 'sql_error_handler';
  DB::$nonsql_error_handler = 'sql_error_handler';

  function sql_error_handler($params) {
      global $app, $log;

      $log->err("SQL error: " . $params['error']);

      if (isset($params['query'])) {
          $log->err(" on query: " . $params['query']);
      }

      header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

      echo json_encode("Internal Server Error");

      die; // don't want to keep going if a query broke
  }

  $app = new Slim\Slim();

  $app->response()->header('content-type', 'application/json');

  \Slim\Route::setDefaultConditions(array(
      'id' => '[1-9]\d*'
  ));

  $app->notFound(function() use ($app) {

      echo json_encode("404 - item not found");
  });

  $app->error(function(\Exception $e) use ($app, $log) {
      $log->err($e); //FIXME: split $e.__toString() output into lines and write them one by one to the log

      echo json_encode("500 - internal server error");
  });

  $app->get("/users", function() use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /users refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $usersList = DB::query("SELECT * FROM users");
      $bundle['cU'] = $userId;
      $bundle['uL'] = $usersList;
      echo json_encode($bundle, JSON_PRETTY_PRINT);
  });

  $app->post('/users', function() use ($app, $log) {
      $json = $app->request()->getBody();
      $data = json_decode($json, true); //true makes it return associative arrays, not objects

      if (($result = isUserValid($data)) === true) {
          DB::insert('users', $data);
          $app->response()->setStatus(201);
          echo json_encode(DB::insertId());
      } else {
          $log->debug("POST / users 400: " . $result);
          $app->response()->setStatus(400);
          echo json_encode("400 - data invalid: " . $result);
      }
  });

  
  $app->get("/messages/:otherUserId", function($otherUserId) use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /messages refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $msgsList = DB::query("SELECT * FROM messages WHERE (fromId=%i AND toId=%i) OR (fromId=%i AND toId=%i)",  $userId, $otherUserId, $otherUserId, $userId);
      echo json_encode($msgsList, JSON_PRETTY_PRINT);
  });

  $app->post('/messages', function() use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /messages refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $json = $app->request()->getBody();
      $data = json_decode($json, true); //true makes it return associative arrays, not objects

      if (($result = isMsgValid($data)) === true) {
          $data['fromId'] = $userId;
          DB::insert('messages', $data);
          $app->response()->setStatus(201);
          echo json_encode(DB::insertId());
      } else {
          $log->debug("POST / messages 400: " . $result);
          $app->response()->setStatus(400);
          echo json_encode("400 - data invalid: " . $result);
      }
  });

//returns TRUE if valid
//returns string describing error if invalid
  function isUserValid($username) {
      if (is_null($username) || empty($username)) {
          return "JSON parsing failed, user is null/empty";
      }

      if (count($username) != 2) {
          return "Invalid number of values";
      }

      if (strlen($username['username']) < 4 || strlen($username['username']) > 20 || !preg_match("/^[A-Za-z0-9_]+$/", $username['username'])) {
          return "Username must be 4-20 characters long, only composed of uppercase/lowercase letters, numbers and underscores";
      }

      if (empty($username['password'])) {
          return "Password shouldn't be empty";
      }

      return TRUE;
  }

//returns TRUE if valid
//returns string describing error if invalid
  function isMsgValid($item) {
      if (is_null($item) || empty($item)) {
          return "JSON parsing failed, user is null/empty";
      }

      if (count($item) != 2) {
          return "Invalid number of values";
      }

      if (empty($item['msg'])) {
          return "Message shouldn't be empty";
      }


      return TRUE;
  }

//returns FALSE if authentication failed or missing
//returns userId if successful
  function getAuthUserId($app) {
      $username = $app->request()->get('username');
      $password = $app->request()->get('password');
      $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);

      $userId = false;

      if ($user) {
          if ($user['password'] == $password) {
              $userId = $user['id'];
          }
      }

      return $userId;
  }

  $app->run();
  