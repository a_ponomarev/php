<?php

  session_start();

  if (!isset($_SESSION['user'])) {
      $_SESSION['user'] = array();
  }

  require_once 'vendor/autoload.php';

  use Monolog\Logger;
  use Monolog\Handler\StreamHandler;

// create a log channel
  $log = new Logger('main');
  $log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
  $log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

  DB::$user = 'quiz2';
  DB::$password = '0I7FdQaY2SIoRCLf';
  DB::$dbName = 'quiz2';
  DB::$port = 3333;
  DB::$encoding = 'utf8';
  DB::$error_handler = 'sql_error_handler';
  DB::$nonsql_error_handler = 'sql_error_handler';

  function sql_error_handler($params) {
      global $app, $log;

      $log->err("SQL error: " . $params['error']);
      $log->err(" on query: " . $params['query']);

      $app->render("error_internal.html.twig", array(
          'error' => $params['error'],
          'query' => $params['query']
      ));

      die; // don't want to keep going if a query broke
  }

// Slim creation and setup
  $app = new \Slim\Slim(array(
      'view' => new \Slim\Views\Twig()
  ));

  $view = $app->view();
  $view->parserOptions = array(
      'debug' => true,
      'cache' => dirname(__FILE__) . '/cache'
  );

  $app->view->parserExtensions = [
      new \Slim\Views\TwigExtension(),
      new \Twig_Extension_Debug()
  ];

  $view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

  $app->get('/isusernameregistered/:username', function($username) {
      if (DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username)) {
          echo "Username already in use";
      } else {
          echo "";
      }
  });

  $app->get('/register', function() use ($app) {
      $app->render('register.html.twig');
  });

  $app->post('/register', function() use ($app, $log) {

      $username = $app->request()->post('username');
      $password = $app->request()->post('password');


//      verify and collect errors
      $errorList = array();

      $valueList = array('username' => $username, 'password' => $password);

      if (strlen($username) < 5 || strlen($username) > 20 || preg_match('/^[0-9]/', $username) || !preg_match("/[A-Z]/", $username) || !preg_match("/[a-z]/", $username) || !preg_match('/[0-9_]/', $username) || preg_match('/[!#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $username)) {
          $valueList['username'] = "";
          array_push($errorList, "Username must be 5-20 characters long, only composed of uppercase/lowercase letters, numbers (except for the first character) and underscore");
      }

      if (!preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d_!#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\])[A-Za-z\d_!#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]{6,}$/', $password)) {
          array_push($errorList, "Passwords should be >6 chars containing at least one digit or special character, uppercases and lowercases");
      }


      //
      if ($errorList) { // FAILED submission
          $app->render('register.html.twig', array(
              'errorList' => $errorList,
              'v' => $valueList
          ));
      } else { // SUCCESFUL submission
          DB::insert('users', array('username' => $username, 'password' => $password));
          $log->info("Registration successful, record number " . DB::insertId());
          $app->render('register_success.html.twig');
      }
  });

  $app->get('/login', function() use ($app) {
      $app->render('login.html.twig');
  });

  $app->post('/login', function() use ($app, $log) {

      $username = $app->request()->post('username');
      $password = $app->request()->post('password');

      $isValid = false;
      $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);

      if ($user) {
          if ($user['password'] == $password) {
              $isValid = true;
          }
      }

      if (!$isValid) {
          $log->info("User login failed, login attempted use " . $username);
          $app->render("login.html.twig", array('error' => true));
      } else {
          unset($user['password']);
          $_SESSION['user'] = $user;
          $log->info("User logged in, record number " . $user['username']);
          $app->render('login_success.html.twig');
      }
  });

  $app->get('/logout', function() use ($app, $log) {
      $log->info("User log off for " . $_SESSION['user']['username']);
      $_SESSION['user'] = array();
      $app->render('logout.html.twig');
  });

  $app->get('/session', function() {
      echo '$_SESSION: <pre>';
      print_r($_SESSION);
  });

  $app->get('/trips/list/', function() use ($app) {
      if (!$_SESSION['user']) {
          $app->render('access_denied.html.twig');
          return;
      }
      //otherwise continue
      $tripsList = DB::query("SELECT * FROM trips WHERE userid=%i", $_SESSION['user']['id']);

      $app->render('trips_list.html.twig', array('tripsList' => $tripsList));
  });

  $app->get('/trips/add', function() use ($app) {
      if (!$_SESSION['user']) {
          $app->render('access_denied.html.twig');
          return;
      }
      //otherwise continue
      $userId = $_SESSION['user']['id'];
      $app->render('trips_add.html.twig');
  });

  $app->post('/trips/add', function() use ($app, $log) {
      if (!$_SESSION['user']) {
          $app->render('access_denied.html.twig');
          return;
      }
      //otherwise continue
      $userId = $_SESSION['user']['id'];
      $date = $app->request()->post('date');
      $cityFrom = $app->request()->post('cityFrom');
      $cityTo = $app->request()->post('cityTo');

//      verify and collect errors
      $errorList = array();

      $valueList = array('date' => $date, 'cityFrom' => $cityFrom, 'cityTo' => $cityTo);

      if ($cityFrom == "") {
          $valueList['cityFrom'] = "";
          array_push($errorList, "City From shouldn't be empty");
      }

      if ($cityTo == "") {
          $valueList['cityTo'] = "";
          array_push($errorList, "City To shouldn't be empty");
      }

      //
      if ($errorList) { // FAILED submission
          $app->render('trips_add.html.twig', array(
              'errorList' => $errorList,
              'v' => $valueList
          ));
      } else { // SUCCESFUL submission
          DB::insert('trips', array('date' => $date, 'cityFrom' => $cityFrom, 'cityTo' => $cityTo, 'userid' => $userId));
          $log->info("Trip added successfully, record number " . DB::insertId());
          $app->render('trip_add_success.html.twig');
      }
  });

  $app->get('/', function() use ($app) {

      $app->render('index.html.twig', array('user' => $_SESSION['user']));
  });

  $app->run();
  