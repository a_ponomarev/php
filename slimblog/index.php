<?php

session_start();

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

DB::$user = 'slimblog';
DB::$password = 'NzkWpTKy4dukSGaS';
DB::$dbName = 'slimblog';
//  DB::$port = 3333;
DB::$encoding = 'utf8';
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'sql_error_handler';

function sql_error_handler($params) {
    global $app, $log;

    $log->err("SQL error: " . $params['error']);
    $log->err(" on query: " . $params['query']);

    $app->render("error_internal.html.twig", array(
        'error' => $params['error'],
        'query' => $params['query']
    ));

    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);

$app->view->parserExtensions = [
    new \Slim\Views\TwigExtension(),
    new \Twig_Extension_Debug()
];

$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

//  $app->get('/article/add', function() use ($app) {
//      if (!$_SESSION['user']) {
//          $app->render('access_denied.html.twig');
//          return;
//      }
//      //otherwise continue
//      $authorId = $_SESSION['user']['id'];
//      //...
//  });

$app->get('/isemailregistered/:email', function($email) {
    if (DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email)) {
        echo "Email already in use";
    } else {
        echo "";
    }
});

$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});

$app->post('/register', function() use ($app, $log) {

    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');

//      verify and collect errors
    $errorList = array();

    $valueList = array('name' => $name, 'email' => $email);

    if (strlen($name) < 2 || strlen($name) > 100) {
        $valueList['name'] = "";
        array_push($errorList, "Name too short, must be at least 2 char long");
    }

//    if (!preg_match('/^.+\@.+\..+$/iD', $email)) {
//        $valueList['email'] = "";
//        array_push($errorList, "Please enter a valid email");
//    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $valueList['email'] = "";
        array_push($errorList, "Email seems invalid");
    } else {
        if (DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email)) {
            array_push($errorList, "Email already is in use, try <a href='/login'>"
                    . "logging in</a> instead.");
        }
    }

    if (strlen($pass2) < 6 || $pass1 != $pass2 || !preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d_!#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\])[A-Za-z\d_!#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]{6,}$/', $pass2)) {
        array_push($errorList, "Passwords should match, be >6 chars containing at least one digit or special character, uppercases and lowercases");
    }


    //
    if ($errorList) { // FAILED submission
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList
        ));
    } else { // SUCCESFUL submission
        DB::insert('users', array('name' => $name, 'email' => $email, 'password' => $pass2));
        $log->info("Registration successful, record number " . DB::insertId());
        $app->render('register_success.html.twig');
    }
});

$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) {

    $email = $app->request()->post('email');
    $password = $app->request()->post('password');

    $isValid = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);

    if ($user) {
        if ($user['password'] == $password) {
            $isValid = true;
        }
    }

    if (!$isValid) {
        $app->render("login.html.twig", array('error' => true));
    } else {
        unset($user['password']);
        $_SESSION['user'] = $user;
        $log->info("User logged in, record number " . $user['id']);
        $app->render('login_success.html.twig');
    }
});

$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();
    $app->render('logout.html.twig');
});

$app->get('/session', function() {
    echo '$_SESSION: <pre>';
    print_r($_SESSION);
});

$app->get('/article/view/:article', function($articleId) use ($app) {
    $article = DB::queryFirstRow("SELECT * FROM articles a INNER JOIN users u ON a.authorId = u.id WHERE a.id=%i", $articleId);
    $app->render('article_view.html.twig', array('article' => $article));
});

$app->get('/article/add', function() use ($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    //otherwise continue
    $authorId = $_SESSION['user']['id'];
    $app->render('article_add.html.twig');
});

$app->post('/article/add', function() use ($app, $log) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    //otherwise continue
    $authorId = $_SESSION['user']['id'];
    $title = $app->request()->post('title');
    $body = $app->request()->post('body');

//      verify and collect errors
    $errorList = array();

    $valueList = array('title' => $title, 'body' => $body);

    if (strlen($title) < 2 || strlen($title) > 500) {
        $valueList['title'] = "";
        array_push($errorList, "Title must be between 2 and 500 char long");
    }

    if (strlen($body) < 2 || strlen($body) > 10000) {
        $valueList['body'] = "";
        array_push($errorList, "Body must be between 2 and 10000 char long");
    }

    //
    if ($errorList) { // FAILED submission
        $app->render('article_add.html.twig', array(
            'errorList' => $errorList,
            'v' => $valueList
        ));
    } else { // SUCCESFUL submission
        DB::insert('articles', array('title' => $title, 'body' => $body, 'authorId' => $authorId));
        $log->info("Post added successfully, record number " . DB::insertId());
        $app->render('article_add_success.html.twig');
    }
});

$app->get('/', function() use ($app) {
    $articlesList = DB::query("SELECT a.id, a.title, a.body, a.tsCreated, u.name FROM articles a INNER JOIN users u ON a.authorId = u.id ");

    $app->render('index.html.twig', array('articlesList' => $articlesList, 'user' => $_SESSION['user']));
});

$app->run();
