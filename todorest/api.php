<?php

  session_start();

  if (!isset($_SESSION['user'])) {
      $_SESSION['user'] = array();
  }

  require_once 'vendor/autoload.php';

  use Monolog\Logger;
  use Monolog\Handler\StreamHandler;

// create a log channel
  $log = new Logger('main');
  $log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
  $log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

  DB::$user = 'todorest';
  DB::$password = 'xUlYzzDOOcxug8Ao';
  DB::$dbName = 'todorest';
//  DB::$port = 3333;
  DB::$encoding = 'utf8';
  DB::$error_handler = 'sql_error_handler';
  DB::$nonsql_error_handler = 'sql_error_handler';

  function sql_error_handler($params) {
      global $app, $log;

      $log->err("SQL error: " . $params['error']);

      if (isset($params['query'])) {
          $log->err(" on query: " . $params['query']);
      }

      header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

      echo json_encode("Internal Server Error");

      die; // don't want to keep going if a query broke
  }

  $app = new Slim\Slim();

  $app->response()->header('content-type', 'application/json');

  \Slim\Route::setDefaultConditions(array(
      'id' => '[1-9]\d*'
  ));

  $app->notFound(function() use ($app) {

      echo json_encode("404 - item not found");
  });

  $app->error(function(\Exception $e) use ($app, $log) {
      $log->err($e); //FIXME: split $e.__toString() output into lines and write them one by one to the log

      echo json_encode("500 - internal server error");
  });

  $app->get("/todos", function() use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /todos refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $todoList = DB::query("SELECT * FROM todos WHERE userId=%i", $userId);
      echo json_encode($todoList, JSON_PRETTY_PRINT);
  });

  $app->get("/todos/:id", function($id) use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /todos refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i AND userId=%i", $id, $userId);

      if (!$todo) {
          $app->response()->setStatus(404);
      }

      echo json_encode($todo, JSON_PRETTY_PRINT);
  });

  $app->post('/todos', function() use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /todos refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $json = $app->request()->getBody();
      $data = json_decode($json, true); //true makes it return associative arrays, not objects

      if (($result = isTodoValid($data)) === true) {
          $data['userId'] = $userId;
          DB::insert('todos', $data);
          $app->response()->setStatus(201);
          echo json_encode(DB::insertId());
      } else {
          $log->debug("POST / todos 400: " . $result);
          $app->response()->setStatus(400);
          echo json_encode("400 - data invalid: " . $result);
      }
  });

  $app->put('/todos/:id', function($id) use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /todos refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      $json = $app->request()->getBody();
      $data = json_decode($json, true); //true makes it return associative arrays, not objects
      if (($result = isTodoValid($data)) === true) {

          if (DB::queryFirstRow("SELECT * FROM todos WHERE id=%i AND userId=%i", $id, $userId)) {
              $data['userId'] = $userId;
              DB::update('todos', $data, "id=%i", $id);
              echo json_encode(true);
          } else {
              $log->debug(sprintf("PUT /todos/%d 400: row not found", $id));
              $app->response()->setStatus(400);
              echo json_encode("400 - bad request (record does not exist)");
          }
      } else {
          $log->debug(sprintf("PUT /todos/%s 400: %s", $id, $result));
          $app->response()->setStatus(400);
          echo json_encode("data invalid: " . $result);
      }
  });

  $app->delete('/todos/:id', function($id) use ($app, $log) {
      $userId = getAuthUserId($app);

      if (!$userId) {
          $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
          $log->debug("GET /todos refused 403 - user/pass invalid");
          echo json_encode("403 - authentication failed");
          return;
      }

      DB::delete('todos', "id=%i AND userId=%i", $id, $userId);
      //deletion of non-existing record is a success and returns 200
      echo json_encode(DB::affectedRows() != 0);
  });

  //returns TRUE if valid
  //returns string describing error if invalid
  function isTodoValid($todo) {
      if (is_null($todo) || empty($todo)) {
          return "JSON parsing failed, todo is null/empty";
      }

      if (count($todo) != 3) {
          return "Invalid number of values";
      }

      if (strlen($todo['task']) < 1 || strlen($todo['task']) > 100) {
          return "Task length is invalid, must be 1-100 chars";
      }

      if (date_create_from_format("Y-m-d", $todo['dueDate'] === FALSE)) {
          return "Due date format is invalid, must be YYYY-MM-DD";
      }

      if ($todo['isDone'] != 'pending' && $todo['isDone'] != 'done') {
          return "isDone must be either 'done' or 'pending'";
      }

      return TRUE;
  }

  //returns FALSE if authentication failed or missing
  //returns userId if successful
  function getAuthUserId($app) {
      $username = $app->request()->get('username');
      $password = $app->request()->get('password');
      $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $username);

      $userId = false;

      if ($user) {
          if ($user['password'] == $password) {
              $userId = $user['id'];
          }
      }

      return $userId;
  }

  $app->run();
  