<?php

session_start();

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

DB::$user = 'hw5auctions';
DB::$password = 'hb4sgEIKiXErrbrx';
DB::$dbName = 'hw5auctions';
//  DB::$port = 3333;
DB::$encoding = 'utf8';
DB::$error_handler = 'sql_error_handler';
DB::$nonsql_error_handler = 'sql_error_handler';

function sql_error_handler($params) {
    global $app, $log;

    $log->err("SQL error: " . $params['error']);

    if (isset($params['query'])) {
        $log->err(" on query: " . $params['query']);
    }

    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);

    echo json_encode("Internal Server Error");

    die; // don't want to keep going if a query broke
}

$app = new Slim\Slim();

$app->response()->header('content-type', 'application/json');

\Slim\Route::setDefaultConditions(array(
    'id' => '[1-9]\d*'
));

$app->notFound(function() use ($app) {

    echo json_encode("404 - item not found");
});

$app->error(function(\Exception $e) use ($app, $log) {
    $log->err($e); //FIXME: split $e.__toString() output into lines and write them one by one to the log

    echo json_encode("500 - internal server error");
});

$app->post('/users', function() use ($app, $log) {

    $json = $app->request()->getBody();
    $data = json_decode($json, true); //true makes it return associative arrays, not objects

    if (($result = isUserValid($data)) === true) {
        DB::insert('users', $data);
        $app->response()->setStatus(201);
        echo json_encode(DB::insertId());
    } else {
        $log->debug("POST / users 400: " . $result);
        $app->response()->setStatus(400);
        echo json_encode("400 - data invalid: " . $result);
    }
});

$app->put('/users/', function() use ($app, $log) {
    $userId = getAuthUserId($app);

    if (!$userId) {
        $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
        $log->debug("GET /users refused 403 - user/pass invalid");
        echo json_encode("403 - authentication failed");
        return;
    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true); //true makes it return associative arrays, not objects

    if (DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $userId)) {
        DB::update('users', $data, "id=%i", $userId);
        echo json_encode(true);
    } else {
        $log->debug(sprintf("PUT /users/%d 400: row not found", $userId));
        $app->response()->setStatus(400);
        echo json_encode("400 - bad request (record does not exist)");
    }
});


//  ------------------------------------------------
$app->get("/items", function() use ($app, $log) {
    $itemsList = DB::query("SELECT i.id, i.description, i.lastBid, ou.username as 'owner', lbuser.username as 'last bidder' FROM `items` i join users ou on i.ownerId = ou.id left join users lbuser on i.lastBidderId = lbuser.id");
    echo json_encode($itemsList, JSON_PRETTY_PRINT);
});

$app->get("/items/:id", function($id) use ($app, $log) {
    $item = DB::queryFirstRow("SELECT i.id, i.description, i.lastBid, ou.username as 'owner', lbuser.username as 'last bidder' FROM `items` i join users ou on i.ownerId = ou.id left join users lbuser on i.lastBidderId = lbuser.id WHERE i.id=%i", $id);
    echo json_encode($item, JSON_PRETTY_PRINT);
});

$app->post('/items', function() use ($app, $log) {
    $userId = getAuthUserId($app);

    if (!$userId) {
        $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
        $log->debug("GET /items refused 403 - user/pass invalid");
        echo json_encode("403 - authentication failed");
        return;
    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true); //true makes it return associative arrays, not objects

    if (($result = isItemValid($data)) === true) {
        $data['ownerId'] = $userId;
        DB::insert('items', $data);
        $app->response()->setStatus(201);
        echo json_encode(DB::insertId());
    } else {
        $log->debug("POST / items 400: " . $result);
        $app->response()->setStatus(400);
        echo json_encode("400 - data invalid: " . $result);
    }
});

$app->put('/items/:id', function($id) use ($app, $log) {
    $userId = getAuthUserId($app);

    if (!$userId) {
        $app->response()->setStatus(403); //FIXME: should really be 401 instead!   
        $log->debug("GET /items refused 403 - user/pass invalid");
        echo json_encode("403 - authentication failed");
        return;
    }

    $json = $app->request()->getBody();
    $data = json_decode($json, true); //true makes it return associative arrays, not objects
    if (($result = isItemBidValid($data)) === true) {
        $editableItem = DB::queryFirstRow("SELECT * FROM items WHERE id=%i", $id);
        if ($editableItem) {
            if ($data['lastBid'] > $editableItem['lastBid']) {
                unset($data['description']);
                $data['lastBidderId'] = $userId;
                DB::update('items', $data, "id=%i", $id);
                echo json_encode(true);
                
            } else {
                $app->response()->setStatus(400);
                echo json_encode("The new bid should be higher than previous one!");
                return;
            }
        } else {
            $log->debug(sprintf("PUT /todos/%d 400: row not found", $id));
            $app->response()->setStatus(400);
            echo json_encode("400 - bad request (record does not exist)");
        }
    } else {
        $log->debug(sprintf("PUT /todos/%s 400: %s", $id, $result));
        $app->response()->setStatus(400);
        echo json_encode("data invalid: " . $result);
    }
});

//returns TRUE if valid
//returns string describing error if invalid
function isUserValid($username) {
    if (is_null($username) || empty($username)) {
        return "JSON parsing failed, user is null/empty";
    }

    if (count($username) != 2) {
        return "Invalid number of values";
    }

    if (strlen($username['username']) < 4 || strlen($username['username']) > 20 || !preg_match("/^[A-Za-z0-9]+$/", $username['username'])) {
        return "Username must be 4-20 characters long, only composed of uppercase/lowercase letters, numbers";
    }

    return TRUE;
}

//returns TRUE if valid
//returns string describing error if invalid
function isItemValid($item) {
    if (is_null($item) || empty($item)) {
        return "JSON parsing failed, user is null/empty";
    }

    if (count($item) != 3) {
        return "Invalid number of values";
    }

    if (empty($item['description'])) {
        return "Description shouldn't be empty";
    }

    if (!is_numeric($item['lastBid']) || $item['lastBid'] < 0) {
        return "Last bid shouldn't be negative and be numeric";
    }

    if (!is_null($item['lastBidderId'])) {
        return "Last bidder shouldn't be provided";
    }

    return TRUE;
}

//returns TRUE if valid
//returns string describing error if invalid
function isItemBidValid($item) {
    if (is_null($item) || empty($item)) {
        return "JSON parsing failed, user is null/empty";
    }

    if (count($item) != 3) {
        return "Invalid number of values";
    }

    return TRUE;
}

//returns FALSE if authentication failed or missing
//returns userId if successful
function getAuthUserId($app) {
    $username = $app->request()->get('username');
    $password = $app->request()->get('password');
    $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);

    $userId = false;

    if ($user) {
        if ($user['password'] == $password) {
            $userId = $user['id'];
        }
    }

    return $userId;
}

$app->run();
