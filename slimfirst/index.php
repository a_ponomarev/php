<?php

  require_once 'vendor/autoload.php';

  use Monolog\Logger;
  use Monolog\Handler\StreamHandler;

// create a log channel
  $log = new Logger('main');
  $log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));
  $log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));

  DB::$user = 'slimfirst';
  DB::$password = 'VNnX1ODmqR40pFuO';
  DB::$dbName = 'slimfirst';
  DB::$port = 3333;
  DB::$encoding = 'utf8';
  DB::$error_handler = 'sql_error_handler';
  DB::$nonsql_error_handler = 'sql_error_handler';

  function sql_error_handler($params) {
      global $app, $log;

      $log->err("SQL error: " . $params['error']);
      $log->err(" on query: " . $params['query']);

      $app->render("error_internal.html.twig", array(
          'error' => $params['error'],
          'query' => $params['query']
      ));

      die; // don't want to keep going if a query broke
  }

  // Slim creation and setup
  $app = new \Slim\Slim(array(
      'view' => new \Slim\Views\Twig()
  ));

  $view = $app->view();
  $view->parserOptions = array(
      'debug' => true,
      'cache' => dirname(__FILE__) . '/cache'
  );
  $view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

  $app->get('/', function() use ($app) {
      $peopleList = DB::query("SELECT *** FROM people");
      $app->render('index.html.twig', array('peopleList' => $peopleList));
  });

  $app->get('/hello/:name', function ($name) {
      echo "Hello, " . $name;
  });

  $app->get('/hello/:name/:age', function ($name, $age) use ($app) {
      DB::insert('people', array('name' => $name, 'age' => $age));

      $app->render('hello.html.twig', array('nnn' => $name, 'aaa' => $age));
  });

  $app->get('/person/add', function() use ($app) {
      $app->render('person_add.html.twig');
  });

  $app->post('/person/add', function() use ($app, $log) {
//      extract submitted data
//      $name = $_POST('name'); - the same as below

      $name = $app->request()->post('name');
      $age = $app->request()->post('age');

//      verify and collect errors
      $errorList = array();

      $valueList = array('name' => $name, 'age' => $age);

      if (strlen($name) < 2) {
          $valueList['name'] = "";
          array_push($errorList, "Name too short, must be at least 2 char long");
      }

      if (strlen($age) == 0 || $age < 0 || $age > 150) {
          $valueList['age'] = "";
          array_push($errorList, "Age must be between 0 and 150");
      }

      //
      if ($errorList) { // FAILED submission
          $app->render('person_add.html.twig', array(
              'errorList' => $errorList,
              'v' => $valueList
          ));
      } else { // SUCCESFUL submission
          DB::insert('people', array('name' => $name, 'age' => $age));
          $log->info("Person added, record number " . DB::insertId());
          $app->render('person_add_success.html.twig');
      }
  });

  $app->run();
  